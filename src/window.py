
from gi.repository import Gtk
from .gi_composites import GtkTemplate


@GtkTemplate(ui='/org/gnome/Handytest/window.ui')
class HandytestWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'HandytestWindow'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.init_template()
